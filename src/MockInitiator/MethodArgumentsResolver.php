<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\MockInitiator;

use Gstarczyk\Mimic\MimicException;
use ReflectionMethod;
use ReflectionParameter;

class MethodArgumentsResolver
{
    /**
     * @param ReflectionMethod $constructor
     * @param ObjectProperty[] $dependencies
     *
     * @return array $arguments
     * @throws MimicException when arguments cannot be resolved
     */
    public function resolveArguments(ReflectionMethod $constructor, iterable $dependencies): iterable
    {
        $arguments = [];
        foreach ($constructor->getParameters() as $parameter) {
            $this->validateParameter($parameter);
            $dependency = $this->getDependencyForParameter($dependencies, $parameter);
            $arguments[] = $dependency->getValue();
        }

        return $arguments;
    }

    private function getDependencyForParameter(iterable $dependencies, ReflectionParameter $parameter): ObjectProperty
    {
        $className = '\\'.$parameter->getClass()->getName();
        $argumentName = $parameter->getName();
        $arguments = $this->filterByClassName($dependencies, $className);
        if (count($arguments) > 1) {
            $arguments = $this->filterByName($arguments, $argumentName);
        }

        if (count($arguments) > 1) {
            throw new MimicException(sprintf('Found more than one dependency for argument "%s"', $argumentName));
        } elseif (count($arguments) == 0) {
            throw new MimicException(sprintf('Cannot find dependency for argument "%s"', $argumentName));
        }

        return $arguments[0];
    }

    private function validateParameter(ReflectionParameter $parameter): void
    {
        if (empty($parameter->getClass())) {
            throw new MimicException('Target objects with non object requirements are not supported');
        }
    }

    /**
     * @param ObjectProperty[] $dependencies
     * @param string $className
     *
     * @return ObjectProperty[]
     */
    private function filterByClassName(iterable $dependencies, string $className): iterable
    {
        $arguments = [];
        foreach ($dependencies as $dependency) {
            if ($dependency->getType() == $className) {
                $arguments[] = $dependency;
            }
        }

        return $arguments;
    }

    /**
     * @param ObjectProperty[] $dependencies
     * @param string $argumentName
     *
     * @return ObjectProperty[]
     */
    private function filterByName(iterable $dependencies, string $argumentName): iterable
    {
        $arguments = [];
        foreach ($dependencies as $dependency) {
            if ($dependency->getName() == $argumentName) {
                $arguments[] = $dependency;
            }
        }

        return $arguments;
    }
}
