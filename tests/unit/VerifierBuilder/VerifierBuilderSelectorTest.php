<?php
namespace Gstarczyk\Mimic\UnitTest\VerifierBuilder;

use Gstarczyk\Mimic\ArgumentsMatchers\ArgumentsMatcherFactory;
use Gstarczyk\Mimic\InvocationRegistry;
use Gstarczyk\Mimic\VerifierBuilder\ConsecutiveInvocations;
use Gstarczyk\Mimic\VerifierBuilder\FilteredInvocations;
use Gstarczyk\Mimic\VerifierBuilder\VerifierBuilderSelector;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class VerifierBuilderSelectorTest extends TestCase
{
    /** @var VerifierBuilderSelector */
    private $selector;

    /** @var InvocationRegistry | MockObject */
    private $invocationRegistry;

    /** @var ArgumentsMatcherFactory | MockObject */
    private $argumentsMatcherFactory;

    /**
     * @throws ReflectionException
     */
    protected function setUp(): void
    {
        $this->argumentsMatcherFactory = $this->createMock(ArgumentsMatcherFactory::class);
        $this->invocationRegistry = $this->createMock(InvocationRegistry::class);
        $this->selector = new VerifierBuilderSelector($this->invocationRegistry, $this->argumentsMatcherFactory);
    }

    public function testMethodReturnFilteredInvocationVerifier()
    {
        $result = $this->selector->method('someMethod');

        Assert::assertInstanceOf(FilteredInvocations\VerifierBuilder::class, $result);
    }

    public function testConsecutiveMethodInvocationsReturnConsecutiveInvocationVerifier()
    {
        $result = $this->selector->consecutiveMethodInvocations('someMethod');

        Assert::assertInstanceOf(ConsecutiveInvocations\VerifierBuilder::class, $result);
    }
}
