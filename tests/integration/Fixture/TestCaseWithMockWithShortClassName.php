<?php
namespace Gstarczyk\Mimic\IntegrationTest\Fixture;

/**
 * test case with single mock
 */
class TestCaseWithMockWithShortClassName
{
    /**
     * @var Mock1
     * @mock
     */
    public $mockedObject;
}