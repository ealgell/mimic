<?php

namespace Gstarczyk\Mimic\Mock;

use Gstarczyk\Mimic\MimicException;
use ReflectionMethod;

class MethodDefinitionFactory
{
    const INVOCATION_HANDLER = '$this->__invocationHandler->handleInvocation(__FUNCTION__, func_get_args())';
    const INVOCATION_HANDLER_WITH_RETURN = 'return '.self::INVOCATION_HANDLER;

    /**
     * @var MethodArgumentFactory
     */
    private $argumentFactory;

    /**
     * @var MethodReturnTypeFactory
     */
    private $returnTypeFactory;

    public function __construct(MethodArgumentFactory $argumentFactory, MethodReturnTypeFactory $returnTypeFactory)
    {
        $this->argumentFactory = $argumentFactory;
        $this->returnTypeFactory = $returnTypeFactory;
    }

    /**
     * @param ReflectionMethod $method
     * @return MethodDefinition
     */
    public function createMethodDefinition(ReflectionMethod $method): MethodDefinition
    {
        if ($method->isFinal()) {
            throw new MimicException(sprintf('Method %s is final', $method->getName()));
        }
        $definition = new MethodDefinition();
        $definition->setArguments($this->argumentFactory->createArgumentsList($method));
        $definition->setMethodName($method->getShortName());

        $returnType = $this->returnTypeFactory->createReturnType($method);
        if ($returnType !== null) {
            $definition->setReturnType($returnType);
        }

        $definition->setInstructions(
            [
                self::INVOCATION_HANDLER_WITH_RETURN,
            ]
        );

        if ($definition->getReturnType() === 'void') {
            $definition->setInstructions(
                [
                    self::INVOCATION_HANDLER,
                ]
            );
        }

        return $definition;
    }
}
