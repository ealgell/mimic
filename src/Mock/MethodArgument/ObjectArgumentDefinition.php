<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\Mock\MethodArgument;

class ObjectArgumentDefinition extends ArgumentDefinition
{
    /** @var  string */
    private $className;

    /**
     * @param string $name
     * @param string $className
     */
    public function __construct(string $name, string $className)
    {
        parent::__construct($name);
        $this->className = $className;
    }

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * @return string
     */
    public function toCode(): string
    {
        $code = sprintf(
            '%s\\%s %s$%s',
            ($this->isNullAllowed() ? '?' : ''),
            $this->className,
            ($this->isPassedByReference() ? '&' : ''),
            $this->getName()
        );
        if ($this->isOptional()) {
            $code .= ' = null';
        }

        return $code;
    }
}
