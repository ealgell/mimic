<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\Mock;

use Gstarczyk\Mimic\MimicException;

class ClassDefinition
{
    /** @var  string */
    private $namespace;

    /** @var  string */
    private $shortName;

    /** @var  string */
    private $extends;

    /** @var string */
    private $implements;

    /** @var ClassAttributeDefinition[] */
    private $attributes = [];

    /** @var MethodDefinition[] */
    private $methodDefinitions = [];

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return $this->namespace.'\\'.$this->shortName;
    }

    /**
     * @return string
     */
    public function getShortName(): string
    {
        return $this->shortName;
    }

    /**
     * @param string $shortName
     */
    public function setShortName(string $shortName): void
    {
        $this->shortName = $shortName;
    }

    /**
     * @return string
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }

    /**
     * @param string $namespace
     */
    public function setNamespace(string $namespace): void
    {
        $this->namespace = trim($namespace, '\\');
    }

    /**
     * @return string
     */
    public function getExtends(): ?string
    {
        return $this->extends;
    }

    /**
     * @param string $extends
     */
    public function setExtends(string $extends): void
    {
        $this->extends = trim($extends, '\\');
    }

    /**
     * @return string
     */
    public function getImplements(): string
    {
        return $this->implements;
    }

    /**
     * @param string $implements
     */
    public function setImplements(string $implements): void
    {
        $this->implements = trim($implements, '\\');
    }

    /**
     * @return ClassAttributeDefinition[]
     */
    public function getAttributes(): iterable
    {
        return $this->attributes;
    }

    /**
     * @param ClassAttributeDefinition[] $attributes
     */
    public function setAttributes(iterable $attributes): void
    {
        $this->attributes = [];
        foreach ($attributes as $attribute) {
            $this->addAttribute($attribute);
        }
    }

    public function addAttribute(ClassAttributeDefinition $attributeDefinition): void
    {
        $this->attributes[] = $attributeDefinition;
    }

    /**
     * @return MethodDefinition[]
     */
    public function getMethodDefinitions(): iterable
    {
        return $this->methodDefinitions;
    }

    /**
     * @param string $methodName
     * @return MethodDefinition
     * @throws MimicException
     */
    public function getMethodDefinition(string $methodName): MethodDefinition
    {
        foreach ($this->methodDefinitions as $methodDefinition) {
            if ($methodDefinition->getMethodName() == $methodName) {
                return $methodDefinition;
            }
        }

        throw new MimicException(sprintf('Unknown method [name="%s"]', $methodName));
    }

    public function addMethodDefinition(MethodDefinition $methodDefinition): void
    {
        $this->methodDefinitions[] = $methodDefinition;
    }

    /**
     * @return string
     */
    public function toCode(): string
    {
        $code = '';
        if ($this->namespace) {
            $code .= 'namespace '.$this->namespace.';'.PHP_EOL;
        }
        $code .= 'class '.$this->shortName;
        if ($this->extends) {
            $code .= ' extends \\'.$this->extends;
        } elseif ($this->implements) {
            $code .= ' implements \\'.$this->implements;
        }
        $code .= PHP_EOL;
        $code .= '{'.PHP_EOL;
        foreach ($this->attributes as $attribute) {
            $code .= $attribute->toCode().PHP_EOL;
        }
        foreach ($this->methodDefinitions as $methodDefinition) {
            $code .= $methodDefinition->toCode().PHP_EOL;
        }
        $code .= '}';

        return $code;
    }
}
