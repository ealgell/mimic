<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\VerifierBuilder\FilteredInvocations;

interface ArgumentsChooser
{
    /**
     * @param $_
     * @return Verification
     */
    public function with($_): Verification;

    /**
     * @return Verification
     */
    public function withAnyArguments(): Verification;

    /**
     * @return Verification
     */
    public function withoutArguments(): Verification;
}
