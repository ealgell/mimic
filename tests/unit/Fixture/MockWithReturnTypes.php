<?php

namespace Gstarczyk\Mimic\UnitTest\Fixture;

use stdClass;

class MockWithReturnTypes
{
    public function methodWithString(): string
    {
        return '';
    }

    public function methodWithInt(): int
    {
        return 1;
    }

    public function methodWithClass(): ClassOne
    {
        return new ClassOne('one', new stdClass());
    }

    public function methodWithSelf(): self
    {
        return $this;
    }
}
