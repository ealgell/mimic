<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\MockInitiator;

use ReflectionClass;

class TargetObjectFactory
{
    /** @var MethodArgumentsResolver */
    private $argumentsResolver;

    public function __construct(MethodArgumentsResolver $argumentsResolver)
    {
        $this->argumentsResolver = $argumentsResolver;
    }

    /**
     * @param ReflectionClass $reflectionClass
     * @param iterable $dependencies
     * @return object
     */
    public function createTargetObject(ReflectionClass $reflectionClass, iterable $dependencies): object
    {
        $arguments = [];
        if ($reflectionClass->hasMethod('__construct')) {
            $constructor = $reflectionClass->getMethod('__construct');
            $arguments = $this->argumentsResolver->resolveArguments($constructor, $dependencies);
        }

        return $reflectionClass->newInstanceArgs($arguments);
    }
}
