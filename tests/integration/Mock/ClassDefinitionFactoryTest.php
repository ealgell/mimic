<?php
namespace Gstarczyk\Mimic\IntegrationTest\Mock;

use Gstarczyk\Mimic\IntegrationTest\Fixture\ClassWithTrait;
use Gstarczyk\Mimic\Mock\ClassDefinition;
use Gstarczyk\Mimic\Mock\ClassDefinitionFactory;
use Gstarczyk\Mimic\Mock\MethodArgumentFactory;
use Gstarczyk\Mimic\Mock\MethodDefinitionFactory;
use Gstarczyk\Mimic\Mock\MethodReturnTypeFactory;
use Gstarczyk\Mimic\UniqueIdGenerator;
use Gstarczyk\Mimic\UnitTest\Fixture\ClassOne;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class ClassDefinitionFactoryTest extends TestCase
{
    /** @var ClassDefinitionFactory */
    private $factory;

    /** @var UniqueIdGenerator | MockObject */
    private $uniqueIdGeneratorMock;

    /**
     * @throws ReflectionException
     */
    protected function setUp(): void
    {
        $this->uniqueIdGeneratorMock = $this->createMock(UniqueIdGenerator::class);
        $this->factory = new ClassDefinitionFactory(
            $this->uniqueIdGeneratorMock,
            new MethodDefinitionFactory(
                new MethodArgumentFactory(),
                new MethodReturnTypeFactory()
            )
        );

        $this->uniqueIdGeneratorMock->method('generateId')->willReturn('Mock');
    }

    public function testFactoryCreateClassDefinitionObject()
    {
        $definition = $this->factory->createClassDefinition(ClassOne::class);

        Assert::assertInstanceOf(ClassDefinition::class, $definition);

        $expected = trim(file_get_contents(__DIR__ . '/../Fixture/ClassOne_mock_code.txt'));
        $this->assertEquals($expected, $definition->toCode());
    }

    public function testCreateClassDefinitionForEmptyClassUsingTrait()
    {
        $definition = $this->factory->createClassDefinition(ClassWithTrait::class);

        Assert::assertInstanceOf(ClassDefinition::class, $definition);

        $expected = trim(file_get_contents(__DIR__ . '/../Fixture/ClassWithTrait_mock_code.txt'));
        $this->assertEquals($expected, $definition->toCode());
    }
}
