<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\Mock;

use Exception;
use Gstarczyk\Mimic\InvocationHandler;
use Gstarczyk\Mimic\MimicException;
use Gstarczyk\Mimic\Mock\MethodArgument\ObjectArgumentDefinition;
use Gstarczyk\Mimic\UniqueIdGenerator;
use InvalidArgumentException;
use ReflectionClass;
use ReflectionMethod;

class ClassDefinitionFactory
{
    /**
     * @var  UniqueIdGenerator
     */
    private $uniqueIdGenerator;

    /**
     * @var MethodDefinitionFactory
     */
    private $methodDefinitionFactory;

    public function __construct(UniqueIdGenerator $uniqueIdGenerator, MethodDefinitionFactory $methodDefinitionFactory)
    {
        $this->uniqueIdGenerator = $uniqueIdGenerator;
        $this->methodDefinitionFactory = $methodDefinitionFactory;
    }

    /**
     * @param string $className
     * @return ClassDefinition
     * @throws InvalidArgumentException when given class cannot be mocked, for ex. class is final
     */
    public function createClassDefinition(string $className): ClassDefinition
    {
        try {
            $class = new ReflectionClass($className);
            if ($class->isFinal()) {
                throw new InvalidArgumentException(
                    sprintf('Cannot mock final class [className=%s]', $class->getName())
                );
            }

            $imitationClassName = $class->getShortName().'_'.$this->uniqueIdGenerator->generateId();
            $definition = new ClassDefinition();
            $definition->setShortName($imitationClassName);
            $definition->setNamespace($class->getNamespaceName());
            $definition->addAttribute(new ClassAttributeDefinition('__invocationHandler'));
            $definition->addMethodDefinition($this->createConstructorDefinition());

            if ($class->isInterface()) {
                $definition->setImplements($class->getName());
            } else {
                $definition->setExtends($class->getName());
            }

            foreach ($class->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
                if ($method->getShortName() !== '__construct' && !$method->isStatic()) {
                    $definition->addMethodDefinition(
                        $this->methodDefinitionFactory->createMethodDefinition($method)
                    );
                }
            }

            return $definition;
        } catch (Exception $exception) {
            throw new MimicException(
                strtr(
                    'Cannot create mock for type "{type}". {reason}',
                    [
                        '{type}' => $className,
                        '{reason}' => $exception->getMessage(),
                    ]
                ), 0, $exception
            );
        }
    }

    /**
     * @return MethodDefinition
     */
    private function createConstructorDefinition(): MethodDefinition
    {
        $definition = new MethodDefinition();
        $definition->setArguments(
            [
                new ObjectArgumentDefinition('invocationHandler', InvocationHandler::class),
            ]
        );
        $definition->setMethodName('__construct');
        $definition->setInstructions(
            [
                '$this->__invocationHandler = $invocationHandler',
            ]
        );

        return $definition;
    }
}
