<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic;

use Gstarczyk\Mimic\TimesVerifiers\AtLeast;
use Gstarczyk\Mimic\TimesVerifiers\Exactly;

class Times
{
    /**
     * @param int $times
     * @return TimesVerifier
     */
    static public function atLeast(int $times): TimesVerifier
    {
        return new AtLeast($times);
    }

    /**
     * @param int $times
     * @return TimesVerifier
     */
    static public function exactly(int $times): TimesVerifier
    {
        return new Exactly($times);
    }

    /**
     * @return TimesVerifier
     */
    static public function once(): TimesVerifier
    {
        return new Exactly(1);
    }

    /**
     * @return TimesVerifier
     */
    static public function never(): TimesVerifier
    {
        return new Exactly(0);
    }
}
