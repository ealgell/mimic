<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\ValueMatchers;

use Gstarczyk\Mimic\ValueMatcher;

class AnyStringMatcher implements ValueMatcher
{
    public function match($value): bool
    {
        return is_string($value);
    }
}
