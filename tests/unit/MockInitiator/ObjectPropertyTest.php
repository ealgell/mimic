<?php
namespace Gstarczyk\Mimic\UnitTest\MockInitiator;

use Gstarczyk\Mimic\MockInitiator\ObjectProperty;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionException;
use ReflectionProperty;
use stdClass;

class ObjectPropertyTest extends TestCase
{
    /** @var ObjectProperty */
    private $object;

    /** @var ReflectionProperty | MockObject */
    private $reflectionProperty;

    /** @var stdClass */
    private $parentObject;

    /**
     * @throws ReflectionException
     */
    protected function setUp(): void
    {
        $this->reflectionProperty = $this->createMock(ReflectionProperty::class);
        $this->parentObject = new stdClass();
        $this->object = new ObjectProperty($this->reflectionProperty, $this->parentObject, 'string', '');
    }

    public function testGetNameReturnNameGotFromReflection()
    {
        $this->reflectionProperty->method('getName')
            ->willReturn('someProperty');

        $result = $this->object->getName();

        $this->assertEquals('someProperty', $result);
    }

    public function testGetValueReturnValueGotFromReflection()
    {
        $this->reflectionProperty->method('getvalue')
            ->willReturn('someValue');

        $result = $this->object->getValue();

        $this->assertEquals('someValue', $result);
    }

    public function testSetValueUseReflectionToSetValue()
    {
        $this->reflectionProperty->expects($this->once())
            ->method('setvalue')
            ->with($this->parentObject, 'someValue');

        $this->reflectionProperty->expects($this->once())
            ->method('setAccessible')
            ->with(true);

        $this->object->setValue('someValue');
    }
}
