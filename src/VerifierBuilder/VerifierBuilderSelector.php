<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\VerifierBuilder;

use Gstarczyk\Mimic\ArgumentsMatchers\ArgumentsMatcherFactory;
use Gstarczyk\Mimic\InvocationRegistry;
use Gstarczyk\Mimic\VerifierBuilder\ConsecutiveInvocations;
use Gstarczyk\Mimic\VerifierBuilder\ConsecutiveInvocations\FirstInvocationArguments;
use Gstarczyk\Mimic\VerifierBuilder\FilteredInvocations\ArgumentsChooser;
use Gstarczyk\Mimic\VerifierBuilder\FilteredInvocations\VerifierBuilder;

class VerifierBuilderSelector
{
    /** @var InvocationRegistry */
    private $invocationRegistry;

    /** @var ArgumentsMatcherFactory */
    private $argumentsMatcherFactory;

    public function __construct(
        InvocationRegistry $invocationRegistry,
        ArgumentsMatcherFactory $matchingArgumentsFactory
    ) {
        $this->invocationRegistry = $invocationRegistry;
        $this->argumentsMatcherFactory = $matchingArgumentsFactory;
    }

    /**
     * @param string $methodName
     * @return ArgumentsChooser
     */
    public function method(string $methodName): ArgumentsChooser
    {
        $verifier = new VerifierBuilder(
            $this->invocationRegistry,
            $this->argumentsMatcherFactory
        );
        $verifier->method($methodName);

        return $verifier;
    }

    /**
     * @param string $methodName
     * @return FirstInvocationArguments
     */
    public function consecutiveMethodInvocations(string $methodName): FirstInvocationArguments
    {
        $verifier = new ConsecutiveInvocations\VerifierBuilder(
            $this->invocationRegistry,
            $this->argumentsMatcherFactory
        );
        $verifier->setMethodName($methodName);


        return $verifier;
    }
}
