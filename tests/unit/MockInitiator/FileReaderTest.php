<?php
namespace Gstarczyk\Mimic\UnitTest\MockInitiator;

use Gstarczyk\Mimic\MimicException;
use Gstarczyk\Mimic\MockInitiator\FileReader;
use PHPUnit\Framework\TestCase;

class FileReaderTest extends TestCase
{
    const FILE_CONTENTS = 'some file content';

    /** @var FileReader */
    private $reader;

    protected function setUp(): void
    {
        $this->reader = new FileReader();
    }

    public function testReaderThrowExceptionWhenCannotReadFileContents()
    {
        $this->expectException(MimicException::class);

        $this->reader->getContents('\path\to\file.ext');
    }
}
