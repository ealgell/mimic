<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\TimesVerifiers;

use Gstarczyk\Mimic\MimicException;

class TimesVerificationException extends MimicException
{

}
