<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\ValueMatchers;

use Gstarczyk\Mimic\ValueMatcher;

class EqualMatcher implements ValueMatcher
{
    /** @var mixed */
    private $value;

    /**
     * @param mixed $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    public function match($value): bool
    {
        return $value == $this->value;
    }
}
