<?php
namespace Gstarczyk\Mimic\UnitTest\MockInitiator;

use Gstarczyk\Mimic\MockInitiator\Context;
use Gstarczyk\Mimic\MockInitiator\ContextFactory;
use Gstarczyk\Mimic\MockInitiator\ObjectProperty;
use Gstarczyk\Mimic\MockInitiator\ObjectPropertyFactory;
use Gstarczyk\Mimic\MockInitiator\PropertyExtractor;
use Gstarczyk\Mimic\UnitTest\Fixture\InitMocks\SimpleTestCase;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class PropertyExtractorTest extends TestCase
{
    /** @var PropertyExtractor */
    private $extractor;

    /** @var ObjectPropertyFactory | MockObject */
    private $propertyFactory;

    /** @var ContextFactory | MockObject */
    private $contextFactory;

    /**
     * @throws ReflectionException
     */
    protected function setUp(): void
    {
        $this->propertyFactory = $this->createMock(ObjectPropertyFactory::class);
        $this->contextFactory = $this->createMock(ContextFactory::class);
        $this->extractor = new PropertyExtractor(
            $this->propertyFactory,
            $this->contextFactory
        );

        $this->propertyFactory
            ->expects($this->any())
            ->method('createObjectProperty')
            ->willReturn($this->createMock(ObjectProperty::class));

        $this->contextFactory->expects($this->any())
            ->method('createContext')
            ->willReturn(new Context(new \stdClass(), '', []));
    }

    /**
     * @throws ReflectionException
     */
    public function testExtractReturnObjectPropertiesCollection()
    {
        $parentObject = new SimpleTestCase();
        $result = $this->extractor->extractProperties($parentObject);

        Assert::assertContainsOnlyInstancesOf(ObjectProperty::class, $result);
    }

    /**
     * @throws ReflectionException
     */
    public function testExtractReturnAllPropertiesDefinedInParentObject()
    {
        $parentObject = new SimpleTestCase();
        $result = $this->extractor->extractProperties($parentObject);

        Assert::assertCount(2, $result);
    }

    /**
     * @throws ReflectionException
     */
    public function testExtractorUsePropertyFactoryToCreateProperties()
    {
        $this->propertyFactory->expects($this->exactly(2))
            ->method('createObjectProperty');

        $parentObject = new SimpleTestCase();
        $this->extractor->extractProperties($parentObject);
    }
}
