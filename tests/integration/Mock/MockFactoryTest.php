<?php
namespace Gstarczyk\Mimic\IntegrationTest\Mock;

use DateTime;
use Exception;
use Gstarczyk\Mimic\InvocationHandler;
use Gstarczyk\Mimic\InvocationRegistry;
use Gstarczyk\Mimic\Mock\ClassDefinitionFactory;
use Gstarczyk\Mimic\Mock\MethodArgumentFactory;
use Gstarczyk\Mimic\Mock\MethodDefinitionFactory;
use Gstarczyk\Mimic\Mock\MethodReturnTypeFactory;
use Gstarczyk\Mimic\Mock\MockFactory;
use Gstarczyk\Mimic\Stub\Stub;
use Gstarczyk\Mimic\UniqueIdGenerator;
use Gstarczyk\Mimic\UnitTest\Fixture\ClassOne;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class MockFactoryTest extends TestCase
{
    /** @var MockFactory */
    private $factory;

    protected function setUp(): void
    {
        $defFactory = new ClassDefinitionFactory(
            new UniqueIdGenerator(),
            new MethodDefinitionFactory(
                new MethodArgumentFactory(),
                new MethodReturnTypeFactory()
            )
        );
        $this->factory = new MockFactory($defFactory);
    }

    /**
     * @throws ReflectionException
     */
    public function testFactoryCreatesMockClassExtendingGivenClass()
    {
        $invocationHandler = $this->createInvocationHandler();
        $result = $this->factory->createMock(ClassOne::class, $invocationHandler);

        $this->assertInstanceOf(ClassOne::class, $result);
    }

    /**
     * @throws Exception
     */
    public function testAllMockedMethodsReturnNull()
    {
        $invocationHandler = $this->createInvocationHandler();

        /** @var ClassOne $result */
        $result = $this->factory->createMock(ClassOne::class, $invocationHandler);

        $this->assertNull($result->publicMethodOne('assad'));
        $this->assertNull($result->publicMethodTwo(new DateTime(), []));
        $this->assertNull($result->publicMethodThree($result));
        $this->assertNull($result->publicMethodFour('str', 100));
    }

    /**
     * @return InvocationHandler
     */
    private function createInvocationHandler()
    {
        $invocationRegistry = new InvocationRegistry();
        $invocationHandler = new InvocationHandler(
            $invocationRegistry, new Stub($invocationRegistry)
        );

        return $invocationHandler;
    }

}
