<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\Stub\Actions;

use Exception;
use Gstarczyk\Mimic\InvocationSignature;
use Gstarczyk\Mimic\Stub\Action;

class ThrowException implements Action
{
    /** @var Exception */
    private $exception;

    public function __construct(Exception $exception)
    {
        $this->exception = $exception;
    }

    /**
     * @param InvocationSignature $invocationSignature
     * @return mixed|void
     * @throws Exception
     */
    public function perform(InvocationSignature $invocationSignature): void
    {
        throw $this->exception;
    }
}
