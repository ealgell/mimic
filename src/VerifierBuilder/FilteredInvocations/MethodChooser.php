<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\VerifierBuilder\FilteredInvocations;

interface MethodChooser
{
    /**
     * @param string $methodName
     * @return ArgumentsChooser
     */
    public function method(string $methodName): ArgumentsChooser;
}
