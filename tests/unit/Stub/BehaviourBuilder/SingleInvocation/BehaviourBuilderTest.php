<?php
namespace Gstarczyk\Mimic\UnitTest\Stub\BehaviourBuilder\SingleInvocation;

use Exception;
use Gstarczyk\Mimic\ArgumentsMatcher;
use Gstarczyk\Mimic\ArgumentsMatchers\AnyArguments;
use Gstarczyk\Mimic\ArgumentsMatchers\ArgumentsMatcherFactory;
use Gstarczyk\Mimic\InvocationMatcher;
use Gstarczyk\Mimic\Stub\Actions\ReturnCallbackResult;
use Gstarczyk\Mimic\Stub\Actions\ReturnValue;
use Gstarczyk\Mimic\Stub\Actions\ThrowException;
use Gstarczyk\Mimic\Stub\Behaviour;
use Gstarczyk\Mimic\Stub\BehaviourBuilder\SingleInvocation\BehaviourBuilder;
use Gstarczyk\Mimic\Stub\Stub;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class BehaviourBuilderTest extends TestCase
{
    /** @var BehaviourBuilder */
    private $builder;

    /** @var Stub | MockObject */
    private $stub;

    /** @var ArgumentsMatcherFactory | MockObject */
    private $matchingArgumentsFactory;

    /**
     * @throws ReflectionException
     */
    protected function setUp(): void
    {
        $this->stub = $this->createMock(Stub::class);
        $this->matchingArgumentsFactory = $this->createMock(ArgumentsMatcherFactory::class);

        $this->builder = new BehaviourBuilder(
            $this->stub,
            $this->matchingArgumentsFactory
        );
    }

    /**
     * @throws ReflectionException
     */
    public function testBuilderUseFactoryForMatchingArgumentsMatcher()
    {
        $argumentsMatcher = $this->createMock(ArgumentsMatcher::class);
        $this->matchingArgumentsFactory
            ->expects($this->once())
            ->method('createMatcher')
            ->willReturn($argumentsMatcher);

        $this->builder->invoke('methodName');
        $this->builder->with('arg');
        $this->builder->willReturn('someValue');
    }

    public function testThenReturnRegisterReturnValueBehaviour()
    {
        $expectedBehaviour = new Behaviour(
            new ReturnValue('someValue'),
            new InvocationMatcher('methodName', new AnyArguments())
        );

        $this->stub->expects($this->once())
            ->method('registerBehaviour')
            ->with($expectedBehaviour);

        $this->builder->invoke('methodName');
        $this->builder->withAnyArguments();
        $this->builder->willReturn('someValue');
    }

    public function testThenReturnCallbackResultRegisterReturnCallbackResultBehaviour()
    {
        $expectedBehaviour = new Behaviour(
            new ReturnCallbackResult(function () {
            }),
            new InvocationMatcher('methodName', new AnyArguments())
        );

        $this->stub->expects($this->once())
            ->method('registerBehaviour')
            ->with($expectedBehaviour);

        $this->builder->invoke('methodName');
        $this->builder->withAnyArguments();
        $this->builder->willReturnCallbackResult(function (){});
    }

    public function testThenReturnCallbackResultRegisterThrowExceptionBehaviour()
    {
        $expectedBehaviour = new Behaviour(
            new ThrowException(new Exception()),
            new InvocationMatcher('methodName', new AnyArguments())
        );

        $this->stub->expects($this->once())
            ->method('registerBehaviour')
            ->with($expectedBehaviour);

        $this->builder->invoke('methodName');
        $this->builder->withAnyArguments();
        $this->builder->willThrow(new Exception());
    }
}
