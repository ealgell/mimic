<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\Mock;

use Gstarczyk\Mimic\MimicException;
use Gstarczyk\Mimic\Mock\MethodArgument\ArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodArgument\ArrayArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodArgument\BuiltInArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodArgument\DefaultValueAware;
use Gstarczyk\Mimic\Mock\MethodArgument\ObjectArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodArgument\VariantArgumentDefinition;
use ReflectionException;
use ReflectionMethod;
use ReflectionParameter;

class MethodArgumentFactory
{
    /**
     * @param ReflectionParameter $parameter
     * @return ArgumentDefinition
     */
    public function createArgument(ReflectionParameter $parameter): ArgumentDefinition
    {
        try {
            if ($parameter->isArray()) {
                $argument = new ArrayArgumentDefinition($parameter->getName());
            } elseif ($className = $this->getClassName($parameter)) {
                $argument = new ObjectArgumentDefinition($parameter->getName(), $className);
            } elseif ($parameter->getType() !== null) {
                $argument = new BuiltInArgumentDefinition($parameter->getName(), $parameter->getType()->getName());
            } else {
                $argument = new VariantArgumentDefinition($parameter->getName());
            }

            if ($parameter->isDefaultValueAvailable() && $argument instanceof DefaultValueAware) {
                $argument->setDefaultValue($parameter->getDefaultValue());
            }

            if ($parameter->isOptional()) {
                $argument->makeOptional();
            }
            if ($parameter->isPassedByReference()) {
                $argument->makePassedByReference();
            }
            if (method_exists($parameter, 'allowsNull') && $parameter->allowsNull() && !$parameter->isOptional()) {
                $argument->makeNullAllowed();
            }

            return $argument;
        } catch (ReflectionException $e) {
            throw new MimicException('Cannot create method argument for mocked class. '.$e->getMessage());
        }
    }

    /**
     * @param ReflectionMethod $method
     * @return ArgumentDefinition[]
     */
    public function createArgumentsList(ReflectionMethod $method): iterable
    {
        $arguments = [];
        foreach ($method->getParameters() as $parameter) {
            $arguments[] = $this->createArgument($parameter);
        }

        return $arguments;
    }

    private function getClassName(ReflectionParameter $parameter): ?string
    {
        try {
            $class = $parameter->getClass();
            $className = $class ? $class->getName() : null;
        } catch (ReflectionException $exception) {
            $text = $parameter->__toString();
            $matches = [];
            preg_match('/\[\s<\w+?>\s([\\\\\w]+)/s', $text, $matches);

            $className = isset($matches[1]) ? $matches[1] : null;
        }

        return $className;
    }
}
