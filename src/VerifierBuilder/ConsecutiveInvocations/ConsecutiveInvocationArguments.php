<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\VerifierBuilder\ConsecutiveInvocations;

interface ConsecutiveInvocationArguments
{
    /**
     * @param $_
     * @return self
     */
    public function thenWith($_): ConsecutiveInvocationArguments;

    /**
     * @return self
     */
    public function thenWithAnyArguments(): ConsecutiveInvocationArguments;

    /**
     * @return self
     */
    public function thenWithoutArguments(): ConsecutiveInvocationArguments;
}
