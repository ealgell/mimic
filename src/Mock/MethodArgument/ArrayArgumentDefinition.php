<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\Mock\MethodArgument;

class ArrayArgumentDefinition extends ArgumentDefinition implements DefaultValueAware
{
    /** @var  array */
    private $defaultValue;

    /**
     * @return array
     */
    public function getDefaultValue(): array
    {
        return $this->defaultValue;
    }

    /**
     * @param array $defaultValue
     */
    public function setDefaultValue($defaultValue): void
    {
        $this->defaultValue = $defaultValue;
    }

    /**
     * @return string
     */
    public function toCode(): string
    {
        $result = sprintf(
            '%sarray %s$%s',
            ($this->isNullAllowed() ? '?' : ''),
            ($this->isPassedByReference() ? '&' : ''),
            $this->getName()
        );

        if ($this->defaultValue !== null) {
            $result .= ' = '.$this->createDefaultValueCode();
        } elseif ($this->isOptional()) {
            $result .= ' = null';
        }

        return $result;
    }

    private function createDefaultValueCode(): string
    {
        $items = [];
        foreach ($this->defaultValue as $item) {
            if (is_string($item)) {
                $items[] = sprintf('\'%s\'', $item);
            } else {
                $items[] = $item;
            }
        }

        return sprintf('[%s]', implode(', ', $items));
    }
}
