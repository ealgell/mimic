<?php
namespace Gstarczyk\Mimic\IntegrationTest\Fixture;

abstract class AbstractMock
{
    abstract public function methodOne();
}
