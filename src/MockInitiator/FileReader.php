<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\MockInitiator;

use Gstarczyk\Mimic\MimicException;

class FileReader
{
    /**
     * @param string $filePath
     * @return string
     * @throws MimicException when file not exist or cannot read file contents
     */
    public function getContents(string $filePath): string
    {
        $this->validateFile($filePath);

        $contents = file_get_contents($filePath);
        if ($contents === false) {
            throw new MimicException(sprintf('Cannot read file content [filePath=%s]', $filePath));
        }

        return $contents;
    }

    private function validateFile(string $filePath): void
    {
        if (!is_file($filePath) || !is_readable($filePath)) {
            throw new MimicException(sprintf('File "%s" not exist', $filePath));
        }
    }

}
