<?php
namespace Gstarczyk\Mimic\UnitTest\Fixture\InitMocks;

/**
 * test case with invalid mock
 */
class TestCase2
{
    /**
     * @var string
     * @mock
     */
    public $mockedObject;
}