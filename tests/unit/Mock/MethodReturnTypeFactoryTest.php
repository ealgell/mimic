<?php

namespace Gstarczyk\Mimic\UnitTest\Mock;

use Gstarczyk\Mimic\Mock\MethodReturnTypeFactory;
use Gstarczyk\Mimic\UnitTest\Fixture\MockWithReturnTypes;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;
use ReflectionException;
use ReflectionMethod;

class MethodReturnTypeFactoryTest extends TestCase
{
    /**
     * @var MethodReturnTypeFactory
     */
    private $factory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->factory = new MethodReturnTypeFactory();
    }

    /**
     * @param string $methodName
     * @param string $expected
     * @throws ReflectionException
     * @dataProvider builtInTypesProvider
     */
    public function testCreateReturnTypeForBuiltInTypes(string $methodName, string $expected): void
    {
        // given
        $method = new ReflectionMethod(MockWithReturnTypes::class, $methodName);

        // when
        $result = $this->factory->createReturnType($method);

        // then
        Assert::assertEquals($expected, $result);
    }

    public function builtInTypesProvider(): array
    {
        return [
            'string' => [
                'method' => 'methodWithString',
                'expected' => 'string',
            ],
            'integer' => [
                'method' => 'methodWithInt',
                'expected' => 'int',
            ],
        ];
    }

    public function testCreateReturnTypeForSelfReference(): void
    {
        // given
        $method = new ReflectionMethod(MockWithReturnTypes::class, 'methodWithSelf');

        // when
        $result = $this->factory->createReturnType($method);

        // then
        Assert::assertEquals('\Gstarczyk\Mimic\UnitTest\Fixture\MockWithReturnTypes', $result);
    }

    public function testCreateReturnTypeForClass(): void
    {
        // given
        $method = new ReflectionMethod(MockWithReturnTypes::class, 'methodWithClass');

        // when
        $result = $this->factory->createReturnType($method);

        // then
        Assert::assertEquals('\Gstarczyk\Mimic\UnitTest\Fixture\ClassOne', $result);
    }
}
