<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\TimesVerifiers;

class Exactly extends AbstractVerifier
{
    public function verify(int $times): void
    {
        if ($this->getExpectedTimes() != $times) {
            throw new TimesVerificationException(
                sprintf(
                    'Expected exactly %d, but %d was given.',
                    $this->getExpectedTimes(),
                    $times
                )
            );
        }
    }
}
