<?php
namespace Gstarczyk\Mimic\UnitTest\MockInitiator;

use Gstarczyk\Mimic\MockInitiator\Context;
use Gstarczyk\Mimic\MockInitiator\ContextFactory;
use Gstarczyk\Mimic\MockInitiator\ImportsExtractor;
use Gstarczyk\Mimic\UnitTest\Fixture\InitMocks\SimpleTestCase;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class ContextFactoryTest extends TestCase
{
    /** @var ContextFactory */
    private $factory;

    /** @var ImportsExtractor | MockObject */
    private $importsExtractor;

    /**
     * @throws ReflectionException
     */
    protected function setUp(): void
    {
        $this->importsExtractor = $this->createMock(ImportsExtractor::class);
        $this->factory = new ContextFactory(
            $this->importsExtractor
        );

        $this->importsExtractor->expects($this->any())
            ->method('getImports')
            ->willReturn([]);
    }

    public function testFactoryCreateContextWithParentObjectAndNamespace()
    {
        $parentObject = new SimpleTestCase();
        $result = $this->factory->createContext($parentObject);

        Assert::assertInstanceOf(Context::class, $result);
        Assert::assertSame($parentObject, $result->getParentObject());
        Assert::assertEquals('\Gstarczyk\Mimic\UnitTest\Fixture\InitMocks', $result->getNamespace());
    }
}
