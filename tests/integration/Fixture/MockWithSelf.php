<?php

namespace Gstarczyk\Mimic\IntegrationTest\Fixture;

class MockWithSelf
{
    public function publicMethodOne($arg1): self
    {
        return $arg1;
    }
}
