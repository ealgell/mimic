<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\VerifierBuilder\FilteredInvocations;

use Gstarczyk\Mimic\TimesVerifier;
use Gstarczyk\Mimic\TimesVerifiers\TimesVerificationException;

interface Verification
{
    /**
     * @param TimesVerifier $timesVerifier
     * @return void
     * @throws TimesVerificationException
     */
    public function wasCalled(TimesVerifier $timesVerifier): void;
}
