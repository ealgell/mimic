<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\ValueMatchers;

use Gstarczyk\Mimic\ValueMatcher;

class AnyObjectMatcher implements ValueMatcher
{
    /** @var string */
    private $className;

    /**
     * @param string $className
     */
    public function __construct(string $className = null)
    {
        $this->className = $className;
    }

    public function match($value): bool
    {
        if (!is_object($value)) {
            return false;
        }
        if ($this->className !== null) {
            return $value instanceof $this->className;
        }

        return true;
    }
}
