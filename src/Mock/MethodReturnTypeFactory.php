<?php

namespace Gstarczyk\Mimic\Mock;

use ReflectionMethod;
use ReflectionType;

class MethodReturnTypeFactory
{
    public function createReturnType(ReflectionMethod $reflectionMethod): ?string
    {
        if (!$reflectionMethod->hasReturnType()) {
            return null;
        }
        $reflectionType = $reflectionMethod->getReturnType();
        $returnType = ($reflectionType->allowsNull() ? '?' : '')
            .($this->requirePrefix($reflectionType) ? '\\' : '');
        $name = (string)$reflectionType;
        if ($name === 'self') {
            $returnType .= $reflectionMethod->getDeclaringClass()->getName();
        } else {
            $returnType .= $name;
        }

        return $returnType;
    }

    private function requirePrefix(ReflectionType $reflectionType): bool
    {
        return !$reflectionType->isBuiltin();
    }
}
