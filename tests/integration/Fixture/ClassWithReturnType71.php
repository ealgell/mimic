<?php
namespace Gstarczyk\Mimic\IntegrationTest\Fixture;


class ClassWithReturnType71
{
    public function methodWithPhp71NullableReturn($arg): ?string
    {
        return $arg;
    }

    public function methodWithPhp71VoidReturn(): void
    {
        return;
    }
    public function methodWithPhp71NullableParametric(?string $arg): ?string
    {
        return $arg;
    }
}
