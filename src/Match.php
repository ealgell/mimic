<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic;

use Gstarczyk\Mimic\ValueMatchers\AnyFloatMatcher;
use Gstarczyk\Mimic\ValueMatchers\AnyIntegerMatcher;
use Gstarczyk\Mimic\ValueMatchers\AnyObjectMatcher;
use Gstarczyk\Mimic\ValueMatchers\AnyStringMatcher;
use Gstarczyk\Mimic\ValueMatchers\AnyTraversableMatcher;
use Gstarczyk\Mimic\ValueMatchers\EqualMatcher;
use Gstarczyk\Mimic\ValueMatchers\SameMatcher;
use Gstarczyk\Mimic\ValueMatchers\StringEndsWith;
use Gstarczyk\Mimic\ValueMatchers\StringStartsWith;

class Match
{
    static public function equal($value): ValueMatcher
    {
        return new EqualMatcher($value);
    }

    static public function same($value): ValueMatcher
    {
        return new SameMatcher($value);
    }

    static public function anyString(): ValueMatcher
    {
        return new AnyStringMatcher();
    }

    static public function anyInteger(): ValueMatcher
    {
        return new AnyIntegerMatcher();
    }

    static public function anyFloat(): ValueMatcher
    {
        return new AnyFloatMatcher();
    }

    static public function anyTraversable(): ValueMatcher
    {
        return new AnyTraversableMatcher();
    }

    static public function anyObject(string $className = null): ValueMatcher
    {
        return new AnyObjectMatcher($className);
    }

    static public function stringStartsWith(string $prefix): ValueMatcher
    {
        return new StringStartsWith($prefix);
    }

    static public function stringEndsWith(string $suffix): ValueMatcher
    {
        return new StringEndsWith($suffix);
    }
}
