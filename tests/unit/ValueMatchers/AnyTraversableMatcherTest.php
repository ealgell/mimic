<?php
namespace Gstarczyk\Mimic\UnitTest\ValueMatchers;

use Gstarczyk\Mimic\ValueMatchers\AnyTraversableMatcher;
use PHPUnit\Framework\TestCase;

class AnyTraversableMatcherTest extends TestCase
{
    /** @var AnyTraversableMatcher */
    private $matcher;

    protected function setUp(): void
    {
        $this->matcher = new AnyTraversableMatcher();
    }

    /**
     * @param int $value
     * @dataProvider matchingValueProvider
     */
    public function testMatcherReturnTrueWhenGivenValueIsTraversable($value)
    {
        $result = $this->matcher->match($value);

        $this->assertTrue($result);
    }

    public function testMatcherReturnFalseWhenGivenValueIsNotTraversable()
    {
        $value = '100';
        $result = $this->matcher->match($value);

        $this->assertFalse($result);
    }

    public function matchingValueProvider()
    {
        return [
            'array' => [[]],
            'iterator' => [new \ArrayIterator([])],
        ];
    }
}
