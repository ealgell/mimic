<?php
namespace Gstarczyk\Mimic\IntegrationTest\VerifierBuilder\ConsecutiveInvocations;

use Gstarczyk\Mimic\ArgumentsMatchers\ArgumentsMatcherFactory;
use Gstarczyk\Mimic\InvocationRegistry;
use Gstarczyk\Mimic\InvocationSignature;
use Gstarczyk\Mimic\VerifierBuilder\ConsecutiveInvocations\VerifierBuilder;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class VerifierBuilderTest extends TestCase
{
    const METHOD_NAME = 'someMethod';

    /** @var VerifierBuilder */
    private $verifier;

    /** @var InvocationRegistry */
    private $invocationRegistry;

    protected function setUp(): void
    {
        $this->invocationRegistry = new InvocationRegistry();
        $this->verifier = new VerifierBuilder(
            $this->invocationRegistry,
            new ArgumentsMatcherFactory()
        );
        $this->verifier->setMethodName(self::METHOD_NAME);
    }

    public function testVerify()
    {
        $this->registerInvocationsWithArguments([
            [10, 'aa'],
            [11, 'bb'],
            ['any arguments'],
            [],
            [100, 'abc'],
        ]);
        $this->verifier
            ->wasCalledWith(10, 'aa')
            ->thenWith(11, 'bb')
            ->thenWithAnyArguments()
            ->thenWithoutArguments()
            ->thenWith(100, 'abc')
        ;

        Assert::assertTrue(true); // only to make assertion visible by phpunit
    }

    /**
     * @param array $argumentsListsCollection collection of arguments list
     */
    private function registerInvocationsWithArguments(array $argumentsListsCollection)
    {
        foreach ($argumentsListsCollection as $arguments) {
            $invocationSignature = new InvocationSignature(self::METHOD_NAME, $arguments);
            $this->invocationRegistry->registerInvocation($invocationSignature);
        }
    }
}
