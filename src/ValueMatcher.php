<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic;

interface ValueMatcher
{
    /**
     * @param mixed $value
     * @return bool
     */
    public function match($value): bool;
}
