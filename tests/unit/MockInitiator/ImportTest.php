<?php
namespace Gstarczyk\Mimic\UnitTest\MockInitiator;

use Gstarczyk\Mimic\MockInitiator\Import;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class ImportTest extends TestCase
{
    public function testGetClassNameReturnFullClassName()
    {
        $import = new Import('\My\Class\Name', 'MyClass');
        $className = $import->getFullClassName();
        Assert::assertEquals('\My\Class\Name\MyClass', $className);
    }

    public function testGetClassNameReturnProperFullClassNameWhenForGlobalNamespace()
    {
        $import = new Import('\\', 'MyClass');
        $className = $import->getFullClassName();
        Assert::assertEquals('\MyClass', $className);
    }

    public function testHasAliasReturnTrueWhenImportHasAlias()
    {
        $import = new Import('\My\Class\Name', 'MyClass', 'SomeAlias');

        Assert::assertTrue($import->hasAlias());
    }

    public function testHasAliasReturnFalsWhenImportHasNotAlias()
    {
        $import = new Import('\My\Class\Name', 'MyClass');

        Assert::assertFalse($import->hasAlias());
    }
}
