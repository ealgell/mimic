<?php
namespace Gstarczyk\Mimic\IntegrationTest\Fixture;

class Mock1
{
    private $arg1;

    /**
     * @var \stdClass
     */
    private $arg2;

    public function __construct($arg1, \stdClass $arg2)
    {
        $this->arg1 = $arg1;
        $this->arg2 = $arg2;
    }

    public function publicMethodOne($arg1)
    {
        return $arg1;
    }
}
