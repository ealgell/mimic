<?php
namespace Gstarczyk\Mimic\IntegrationTest\Fixture;

class MockWithThisUsage
{
    private $value;

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}
