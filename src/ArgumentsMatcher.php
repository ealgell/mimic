<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic;

interface ArgumentsMatcher
{
    public function match(array $arguments): bool;
}
