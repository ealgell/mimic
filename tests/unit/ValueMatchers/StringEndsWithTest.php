<?php
namespace Gstarczyk\Mimic\UnitTest\ValueMatchers;

use Gstarczyk\Mimic\ValueMatchers\StringEndsWith;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class StringEndsWithTest extends TestCase
{
    public function testMatcherReturnTrueWhenGivenValueEndsWithSpecifiedString()
    {
        $specification = 'Bar';
        $value = 'fooBar';
        $matcher = new StringEndsWith($specification);
        $result = $matcher->match($value);

        Assert::assertTrue($result);
    }

    public function testMatcherReturnFalseWhenGivenValueIsNotEndsWithSpecifiedString()
    {
        $specification = 'foo';
        $value = 'fooBar';
        $matcher = new StringEndsWith($specification);
        $result = $matcher->match($value);

        Assert::assertFalse($result);
    }
}
