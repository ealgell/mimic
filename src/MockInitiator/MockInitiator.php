<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\MockInitiator;

use Gstarczyk\Mimic\MimicException;
use Gstarczyk\Mimic\MimicRegistry;
use ReflectionClass;
use ReflectionException;

class MockInitiator
{
    /** @var MimicRegistry */
    private $mimicRegistry;

    /** @var TargetObjectFactory */
    private $targetObjectFactory;

    /** @var PropertyExtractor */
    private $propertyExtractor;

    public function __construct(
        MimicRegistry $mimicRegistry,
        PropertyExtractor $propertyExtractor,
        TargetObjectFactory $targetObjectFactory
    ) {
        $this->mimicRegistry = $mimicRegistry;
        $this->propertyExtractor = $propertyExtractor;
        $this->targetObjectFactory = $targetObjectFactory;
    }

    /**
     * Create mocks for annotated properties of given test case
     * @param object $testCase
     */
    public function initMocks(object $testCase): void
    {
        try {
            $properties = $this->propertyExtractor->extractProperties($testCase);
            $mocks = $this->createMocks($properties);
            $this->injectMocks($properties, $mocks);
        } catch (ReflectionException $e) {
            throw new MimicException('Cannot initiate mocks: '.$e->getMessage());
        }
    }

    /**
     * @param ObjectProperty[] $properties
     * @return object[]
     */
    private function createMocks(iterable $properties): iterable
    {
        $mockCandidates = array_filter(
            $properties,
            function (ObjectProperty $property) {
                return $property->isMarkedAsMock();
            }
        );
        foreach ($mockCandidates as $mockCandidate) {
            $mock = $this->createMock($mockCandidate);
            $mockCandidate->setValue($mock);
        }

        return $mockCandidates;
    }

    /**
     * @param ObjectProperty $property
     * @return object
     */
    private function createMock(ObjectProperty $property): object
    {
        $this->validateProperty($property);

        return $this->mimicRegistry->getMock($property->getType());
    }

    /**
     * @param ObjectProperty[] $properties
     * @param ObjectProperty[] $mocks
     * @throws ReflectionException
     */
    private function injectMocks(iterable $properties, iterable $mocks): void
    {
        $targetCandidates = array_filter(
            $properties,
            function (ObjectProperty $property) {
                return $property->isMarkedAsMocksTarget();
            }
        );
        foreach ($targetCandidates as $targetCandidate) {
            $target = $this->createTarget($targetCandidate, $mocks);
            $targetCandidate->setValue($target);
        }
    }

    /**
     * @param ObjectProperty $property
     * @param ObjectProperty[] $dependencies
     * @return object
     * @throws ReflectionException
     */
    private function createTarget(ObjectProperty $property, array $dependencies): object
    {
        $this->validateProperty($property);
        $reflectionClass = new ReflectionClass($property->getType());

        return $this->targetObjectFactory->createTargetObject($reflectionClass, $dependencies);
    }

    /**
     * @param ObjectProperty $property
     * @throws MimicException
     */
    private function validateProperty(ObjectProperty $property): void
    {
        $type = $property->getType();
        if (!$type) {
            throw new MimicException('Cannot auto-create mock. Property type is missing!');
        }
    }
}
