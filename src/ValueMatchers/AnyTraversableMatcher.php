<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\ValueMatchers;

use Gstarczyk\Mimic\ValueMatcher;
use Traversable;

class AnyTraversableMatcher implements ValueMatcher
{
    public function match($value): bool
    {
        return is_array($value) || ($value instanceOf Traversable);
    }
}
